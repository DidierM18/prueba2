package com.example.demo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RepositoryMochila extends JpaRepository<ModelMochila,Long> {
	//@Query("SELECT t.modelo FROM mochila t where t.Id =:id")
	//String findModeloById(@Param("id") Long id);

	@Query("SELECT u FROM ModelMochila u WHERE u.id= :id")
	public ModelMochila findMochila(@Param("id") long id);
}
