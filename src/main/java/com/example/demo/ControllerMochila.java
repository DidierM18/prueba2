package com.example.demo;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/persona")
public class ControllerMochila {
	@Autowired
	RepositoryMochila repository;
	List<ModelMochila> list;
	
	//Buscar forma Query 
	@RequestMapping(value = "/buscar/{id}", method = { RequestMethod.GET }, produces = { MediaType.APPLICATION_JSON_VALUE})
	public ModelMochila findAll(@PathVariable("id") int id){
		return repository.findMochila(id);
	}
}
