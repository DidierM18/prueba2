package com.example.demo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.stereotype.Service;

@Service
@Entity
@Table(name = "mochila")
public class ModelMochila implements Serializable{
	private static final long serialVersionUID = -3009157732242241606L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(name = "modelo")
	private String modelo;
	
	@Column(name="color")
	private String color;
	
	@Column(name="material")
	private String material;
	
	@Column(name="Id_persona")
	private int idpersona;
	
	protected ModelMochila()
	{	
	}
	
	public ModelMochila(int id,String modelo,String color, String material,int idPersona)
	{
	
		this.id = id; this.modelo = modelo; this.color = color; this.material = material;this.idpersona = idPersona;
		
	}
	public long getId()
	{
		return id;
	}
	public String getModelo()
	{
		return modelo;
	}
	public String getColor()
	{
		return color;
	}
	public String getMaterial()
	{
		return material;
	}
	public Integer getIdPersona()
	{
		return idpersona;
	}
	
	public void setModelo(String modelo)
	{
		this.modelo =modelo;
	}
	public void setColor(String color)
	{
		this.color = color;
	}
	public void setMaterial(String material)
	{
		this.material = material;
	}
	public void setIdPersona(int idPersona)
	{
		this.idpersona = idPersona;
	}
}
